<!DOCTYPE html>
<head>
<title>Battlefield Analysis</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 
<h1>Battlefield Analysis</h1>
<h2>Latest Critiques</h2>

    <?php
        require "database_access.php";
        //get 5 latest battles
        $stmt = $mysqli->prepare("select ammunition, soldiers, duration, critique, id from reports order by posted DESC");
            $stmt->execute();
            $stmt->bind_result($ammo, $soldiers, $duration, $critique, $id);
        $count = 0;
        while ($stmt->fetch() && ($count<5)){
            $count++;
            //echo $count;
            echo "<div class='battleContainer'>";
                echo "<ul>";
                echo "Ammunition: ".$ammo."  ";
                echo "Soldiers: ".$soldiers."  ";
                echo "Duration: ".$duration."  ";
                echo "Critique: ".$critique."  ";
                echo "</ul><br><br>";

            echo "</div>";

        }
        $stmt->close();
        
        
    ?> 
 
 <h2>Battle Statistics</h2>
 
     <?php
        require "database_access.php";
        
        $stmt = $mysqli->prepare("select ammunition, soldiers, duration, critique, id from reports order by posted DESC");
            $stmt->execute();
            $stmt->bind_result($ammo, $soldiers, $duration, $critique, $id);
        $count = 0;
        while ($stmt->fetch() && ($count<5)){
            $count++;
            //echo $count;
            echo "<div class='battleContainer'>";
                echo "<ul>";
                echo "Ammunition: ".$ammo."  ";
                echo "Soldiers: ".$soldiers."  ";
                echo "Duration: ".$duration."  ";
                echo "Critique: ".$critique."  ";
                echo "</ul><br><br>";

            echo "</div>";

        }
        $stmt->close();
        
        
    ?> 
 
 
</div></body>
</html>