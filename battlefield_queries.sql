Last login: Wed Oct  2 13:27:56 on ttys000
Williams-MacBook-Pro:~ blake$ defaults write com.apple.Finder AppleShowAllFiles TRUE
Williams-MacBook-Pro:~ blake$ 
Last login: Mon Oct  7 01:22:23 on console
Williams-MacBook-Pro:~ blake$ ssh ec2-54-200-63-85.us-west-2.compute.amazonaws.comblake@ec2-54-200-63-85.us-west-2.compute.amazonaws.com's password: 
Welcome to Ubuntu 13.04 (GNU/Linux 3.8.0-19-generic x86_64)

 * Documentation:  https://help.ubuntu.com/

  System information as of Mon Oct  7 11:36:50 CDT 2013

  System load:  0.0               Processes:           78
  Usage of /:   15.6% of 7.75GB   Users logged in:     0
  Memory usage: 40%               IP address for eth0: 172.31.13.69
  Swap usage:   0%

  Graph this data and manage this system at https://landscape.canonical.com/

  Get cloud support with Ubuntu Advantage Cloud Guest:
    http://www.ubuntu.com/business/services/cloud

  Use Juju to deploy your cloud instances and workloads:
    https://juju.ubuntu.com/#cloud-raring

106 packages can be updated.
62 updates are security updates.

Last login: Wed Oct  2 11:27:46 2013 from meru-wufi-s-block16-pat.nts.wustl.edu
blake@ip-172-31-13-69:~$ mysql
ERROR 1045 (28000): Access denied for user 'blake'@'localhost' (using password: NO)
blake@ip-172-31-13-69:~$ mysql -u blake -p news
Enter password: 
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 2614
Server version: 5.5.32-0ubuntu0.13.04.1 (Ubuntu)

Copyright (c) 2000, 2013, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create database battlefield-queries.sql
    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '-queries.sql' at line 1
mysql> create database battlefield-queries;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '-queries' at line 1
mysql> create database 'battlefield-queries';
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''battlefield-queries'' at line 1
mysql> create database battlefield;
Query OK, 1 row affected (0.00 sec)

mysql> create table reports(id mediumint unsigned not null auto_increment, ammunition smallint unsigned not null, soldiers smallint unsigned not null, duration decimal(6,1) unsigned not null, critique tinytext, posted timestamp not null default CURRENT_TIMESTAMP), PRIMARY KEY(id)) engine = INNODB default character set = utf8 collate = utf8_general_ci;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ' PRIMARY KEY(id)) engine = INNODB default character set = utf8 collate = utf8_ge' at line 1
mysql> create table reports(id mediumint unsigned not null auto_increment, ammunition smallint unsigned not null, soldiers smallint unsigned not null, duration decimal(6,1) unsigned not null, critique tinytext, posted timestamp not null default CURRENT_TIMESTAMP, PRIMARY KEY(id)) engine = INNODB default character set = utf8 collate = utf8_general_ci;
Query OK, 0 rows affected (0.02 sec)

mysql> describe reports;
+------------+-----------------------+------+-----+-------------------+----------------+
| Field      | Type                  | Null | Key | Default           | Extra          |
+------------+-----------------------+------+-----+-------------------+----------------+
| id         | mediumint(8) unsigned | NO   | PRI | NULL              | auto_increment |
| ammunition | smallint(5) unsigned  | NO   |     | NULL              |                |
| soldiers   | smallint(5) unsigned  | NO   |     | NULL              |                |
| duration   | decimal(6,1) unsigned | NO   |     | NULL              |                |
| critique   | tinytext              | YES  |     | NULL              |                |
| posted     | timestamp             | NO   |     | CURRENT_TIMESTAMP |                |
+------------+-----------------------+------+-----+-------------------+----------------+
6 rows in set (0.00 sec)

mysql> create user 'bot'@'localhost' identified by 'newpassword';
ERROR 1396 (HY000): Operation CREATE USER failed for 'bot'@'localhost'
mysql> create user bot@'localhost' identified by 'newpassword';
ERROR 1396 (HY000): Operation CREATE USER failed for 'bot'@'localhost'
mysql> use battlefield;
Database changed
mysql> create user bot@'localhost' identified by 'newpassword';
ERROR 1396 (HY000): Operation CREATE USER failed for 'bot'@'localhost'
mysql> create user 'bot'@'localhost' identified by 'newpassword';
ERROR 1396 (HY000): Operation CREATE USER failed for 'bot'@'localhost'
mysql> create user 'robot'@'localhost' identified by 'newpassword';
Query OK, 0 rows affected (0.00 sec)

mysql> grant select,insert,update,delete on battlefield to robot@'localhost';
ERROR 1146 (42S02): Table 'battlefield.battlefield' doesn't exist
mysql> grant select,insert,update,delete on battlefield.reports to robot@'localhost';
ERROR 1146 (42S02): Table 'battlefield.reports' doesn't exist
mysql> grant select,insert,update,delete on reports to robot@'localhost';
ERROR 1146 (42S02): Table 'battlefield.reports' doesn't exist
mysql> use battlefield;
Database changed
mysql> describe reports;
ERROR 1146 (42S02): Table 'battlefield.reports' doesn't exist
mysql> describe report;
ERROR 1146 (42S02): Table 'battlefield.report' doesn't exist
mysql> describe battlefield;
ERROR 1146 (42S02): Table 'battlefield.battlefield' doesn't exist
mysql> create table reports(id mediumint unsigned not null auto_increment, ammunition smallint unsigned not null, soldiers smallint unsigned not null, duration decimal(6,1) unsigned not null, critique tinytext, posted timestamp not null default CURRENT_TIMESTAMP, PRIMARY KEY(id)) engine = INNODB default character set = utf8 collate = utf8_general_ci;
Query OK, 0 rows affected (0.00 sec)

mysql> describe reports;+------------+-----------------------+------+-----+-------------------+----------------+| Field      | Type                  | Null | Key | Default           | Extra          |
+------------+-----------------------+------+-----+-------------------+----------------+
| id         | mediumint(8) unsigned | NO   | PRI | NULL              | auto_increment |
| ammunition | smallint(5) unsigned  | NO   |     | NULL              |                |
| soldiers   | smallint(5) unsigned  | NO   |     | NULL              |                |
| duration   | decimal(6,1) unsigned | NO   |     | NULL              |                |
| critique   | tinytext              | YES  |     | NULL              |                |
| posted     | timestamp             | NO   |     | CURRENT_TIMESTAMP |                |
+------------+-----------------------+------+-----+-------------------+----------------+
6 rows in set (0.00 sec)

mysql> grant select,insert,update,delete on battlefield.reports to robot@'localhost';Query OK, 0 rows affected (0.00 sec)
mysql> select * from reports;
Empty set (0.00 sec)

mysql> insert into results (ammo, soldiers, duration, critique) values (1,3,1,'hello world');
ERROR 1146 (42S02): Table 'battlefield.results' doesn't exist
mysql> insert into reports (ammo, soldiers, duration, critique) values (1,3,1,'hello world');
ERROR 1054 (42S22): Unknown column 'ammo' in 'field list'
mysql> insert into reports (ammunition, soldiers, duration, critique) values (1,3,1,'hello world');
Query OK, 1 row affected (0.00 sec)

mysql> select * from reports;
+----+------------+----------+----------+-------------+---------------------+
| id | ammunition | soldiers | duration | critique    | posted              |
+----+------------+----------+----------+-------------+---------------------+
|  1 |          1 |        3 |      1.0 | hello world | 2013-10-07 12:41:59 |
+----+------------+----------+----------+-------------+---------------------+
1 row in set (0.01 sec)

mysql> select * from reports;
+----+------------+----------+----------+----------------------------+---------------------+
| id | ammunition | soldiers | duration | critique                   | posted              |
+----+------------+----------+----------+----------------------------+---------------------+
|  1 |          1 |        3 |      1.0 | hello world                | 2013-10-07 12:41:59 |
|  2 |          3 |        1 |      3.0 | This was the first battle. | 2013-10-07 12:42:25 |
+----+------------+----------+----------+----------------------------+---------------------+
2 rows in set (0.00 sec)

mysql> select ammunition, soldiers, duration, critique from reports order by posted DESC;
+------------+----------+----------+----------------------------+
| ammunition | soldiers | duration | critique                   |
+------------+----------+----------+----------------------------+
|          3 |        1 |      3.0 | This was the first battle. |
|          1 |        3 |      1.0 | hello world                |
+------------+----------+----------+----------------------------+
2 rows in set (0.00 sec)

mysql> 
x